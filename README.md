# portfolio

Ejemplos de trabajos para el portfolio freelance

## Balanceo de servidores LAMP

Trabajo 1

1. [Server Load Balancer](https://docs.google.com/presentation/d/e/2PACX-1vRpZOLM5jPScLVO2H9CAdNagsrk6ullwQiuCxaODmX5QkLMEpxqktY7xWOoDjctPOZtXwYZcWdQyQ2X/pub?start=true&loop=false&delayms=3000)
1. [On Premise to GKE](https://docs.google.com/presentation/d/e/2PACX-1vSEthOAXfzf97N8gNiXwwoeZ8QK4OO-fIrMZWYSiHViW3QYOZISI6OnEXXYnVrxgF5UjplPNppBCc5A/pub?start=true&loop=false&delayms=10000)
1. [DevOps](https://docs.google.com/presentation/d/e/2PACX-1vRdBPL5VRpj7XzZu5BqDMpieOa0R9i4TF3ulb5DJx31mgUav98gIk9KrLCAhNvzVMDJSmGuEaDexbMO/pub?start=true&loop=false&delayms=3000)
1. [Blockchain](https://docs.google.com/presentation/d/e/2PACX-1vQ3FdYLwbyaz8YeKvPs2W_jemZ_oKUYtHvpaoyPn52Z-RSanIQn4EI1zJuynf4axXA69ny9UR6UQ2OR/pub?start=true&loop=false&delayms=3000)